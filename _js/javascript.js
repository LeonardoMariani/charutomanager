function login() {
    var user = document.getElementById("usuario").value;
    var senha = document.getElementById("senha").value;
    var campos = document.getElementById("todosCampos");
    
    var nomeUsr = localStorage.getItem("nome");
    var emailUsr = localStorage.getItem("email");
    var senhaUsr = localStorage.getItem("senha");
        
    if (user !== "" && senha !== ""){        
        campos.style.color = "white";
        
        //if(user == "master@teste.com" && senha == "abcd1234"){
        if(user == "1" && senha == "1"){
            //alert("Senha correta!"); 
            document.location.href = "temperatura.html";
        }else if(user == nomeUsr && senha == senhaUsr){
            document.location.href = "humidade.html";
        }
    }else{
        
        campos.style.color = "red";
    }
}

function Cadastrar(){
    var nome = document.getElementById("user").value;
    var email = document.getElementById("email").value;
    var senha = document.getElementById("senha").value;
    var campos = document.getElementById("cadastro-campos");
    
    if(nome != "" && email != "" && senha != ""){
        campos.style.color = "white";
        
        localStorage.setItem("nome", nome);
        localStorage.setItem("email", email);
        localStorage.setItem("senha", senha);
        
        alert("Cadastro realizado com sucesso");
        
        document.getElementById("user").value = "";
        document.getElementById("email").value = "";
        document.getElementById("senha").value = "";
                             
  /*        
        if(email.indexOf("@") != -1){
            alert("Email ok");
        }else{
            alert("Email errado");            
        }
      
        if(senha.length > 5){
            var regExp = /^(?=.*[a-zA-Z])(?=.*[0-9])/;
           if(senha.match(regExp)){
               alert("senha Ok");
           }else{
               alert("senha não Ok");
           }
            
        }else{
            alert("Senha não Ok");
        }
        */
    }else{
        campos.style.color = "red";  
    }        
}

function enviarEmail(){
    
    var email = document.getElementById("emailCadastro").value;
    var campoOk = document.getElementById("recuperar-campos");
    
    if(email.length > 0){
        document.location.href = "mailto:JAMIL@MAMOL.com?subject=Recuperar senha";    
        campoOk.style.color = "white";
    }else{
        campoOk.style.color = "red";
    }        
}
Highcharts.ajax({
    url: 'temp.txt',
    dataType: 'text',
    success: function(data) {
        
    var dataFinal = [];     
    var lines = data.split('\n');
    $.each(lines, function(lineNo, line) {
        var items = line.split(','); 
        //alert(items);
        dataFinal.push(parseFloat(items));
    });
     var options = {
             chart:{
               renderTo: 'container'  
             },
            title: {
                text: 'Medição interna da caixa de charutos'
            },

            xAxis: {
                tickInterval: 1,
                title: {
                    text: '<b>Medições</b>'
                }
            },
            yAxis: {
                title: {
                    text: 'Temperatura'
                },
                labels: {
                    formatter: function () {
                        return this.value + '°';
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>Temperatura</b><br/>'
            },
            series: [{
                name: "Sensor",
                pointStart:1,
                data: dataFinal,
                zones: [{
                    value: 12,
                    color: '#1078ef'
                }, {
                    value: 18,
                    color: '#0f13ef'
                }, {
                    value: 25,
                    color: '#ef3b0f'
                }, {
                    color: '#ff0000'
                }]
            }]
        };        
        Highcharts.chart('temperatura', options);
    },
    error: function (e, t) {
        console.error(e, t);
    }
});


Highcharts.ajax({
    url: 'humidade.txt',
    dataType: 'text',
    success: function(data) {
        
    var dataFinal = [];     
    var lines = data.split('\n');
    $.each(lines, function(lineNo, line) {
        var items = line.split(','); 
        //alert(items);
        dataFinal.push(parseFloat(items));
    });
     var options = {
             chart:{
               renderTo: 'container'  
             },
            title: {
                text: 'Medição interna da caixa de charutos'
            },

            xAxis: {
                tickInterval: 1,
                title: {
                    text: '<b>Medições</b>'
                }
            },
            yAxis: {
                title: {
                    text: 'Humidade'
                },
                labels: {
                    formatter: function () {
                        return this.value + '°';
                    }
                },
                max: 100
            },
            tooltip: {
                headerFormat: '<b>Humidade</b><br/>'
            },
            series: [{
                name: "Sensor",
                pointStart:1,
                data: dataFinal,
                zones: [{
                    value: 12,
                    color: '#f7a35c'
                }, {
                    value: 15,
                    color: '#7cb5ec'
                }, {
                    color: '#90ed7d'
                }]
            }]
        };        
        Highcharts.chart('humidade', options);
    },
    error: function (e, t) {
        console.error(e, t);
    }
});
